var IAB_PAGE_URL = "https://bleachr-fe-dev.herokuapp.com/sidearm/?account=syracuse&saToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ1c2VyIjp7IklkIjo0OTMyLCJGdWxsTmFtZSI6Ik1heCBHcmVlbmJlcmciLCJQaWN0dXJlIjpudWxsfSwic3ViIjoic3lyYWN1c2U6NDkzMiIsImlhdCI6MTU1NjEyOTkyMywibmFtZSI6Ik1heCBHcmVlbmJlcmciLCJlbWFpbCI6Im1heGdAaW50ZXJuZXRjb25zdWx0LmNvbSIsInBpY3R1cmUiOm51bGwsImp0aSI6ImZhNzM0Nzk1LWUyZjItNDM3Zi1iYjJlLTNlZmU2ZTg5MzAwNCJ9.LDlTNLa3uJ_nTFbMkd6FW2KJSY_7pnHil2LjbzIGqBn_aOIifjn58S20yaIJQOEiXd1gvCXvg5OgqDlioxeIeJIXdEvJnoGdUzq08B1ckkklH0MH5miQeH-y7yss84sbL70Idv7mIJZZwpcqdntTGaJ1X6EQy54TgOXs7vO_GOb8sc8u_PMC6DxZAEbdcdhHHnnFMgJULIgv8lFogqi9Czvj4Ls3zaY3EoUKBmEmRQ0d_vySL5a8bN8OFxjv16MJCQBP68cWS4JNm8fVvTovG2VQaMgzDHmBE814ituujib1OGR5lm1KWM-F3lYOabEhfPPcOqS7YX3mi7iPgHjohA&latitude=44.977&longitude=-93.259";
// var IAB_PAGE_URL = "https://dpa99c.github.io/cordova-plugin-inappbrowser-test/iab_content/iab.html";

var webView, osVersion, iab, targetWebview, CAMERA, img, pic = false;

function log(msg, className){
    className = className || '';
    console.log(msg);
    $('#log').append('<p class="'+className+'">'+msg+'</p>');
}

function error(msg){
    log(msg, "error");
}

function openIAB(){
    var iabOpts = getIabOpts();

    var logmsg = "Opening IAB";
    if(device.platform === "iOS"){
        if(targetWebview === "wkwebview"){
            logmsg += " using WKWebView";
        }else{
            logmsg += " using UIWebView";
        }
    }
    logmsg += " with opts: "+iabOpts;
    log(logmsg);

    iab = cordova.InAppBrowser.open(IAB_PAGE_URL, '_blank', iabOpts);

    iab.addEventListener('loadstart', function(e) {
        log("received 'loadstart' event");
        console.log("received 'loadstart' event for: " + e.url);
    });
    iab.addEventListener('loadstop', function(e) {
    //     if(pic){
    //         var imgScript = "var p = document.createElement('p');"+
    //                         "p.setAttribute('id', 'sidearm');"
    //                         "p.style.display = 'none';" +
    //                         "p.appendChild(document.createTextNode("+ img +"); "+
    //                         "document.querySelector('body').appendChild(p);" +
    //                         "var event = new Event('sidearmImg'); "+
    //                         "console.log('dispatching');" +
    //                         "p.dispatchEvent(event);";
    //         iab.executeScript({ code: imgScript });
    //         pic = false;
    // }
        log("received 'loadstop' event");
        console.log("received 'loadstop' event for: " + e.url);
    });
    iab.addEventListener('loaderror', function(e) {
        log("received 'loaderror' event");
        error("loaderror: " + e.message);
        console.error("received 'loaderror' event for: " + e.url);
    });
    iab.addEventListener('exit', function () {
        log("received 'exit' event");
    });
    iab.addEventListener('message', function (e) {
        log("received 'message' event");
        console.dir(e);
        if(e.data.action === 'camera'){
            openCamera();
        }
    });
}

// function openCamera() {
//     var animationDelay = 500; // delay to wait for camera window animation
//     navigator.camera.getPicture(function(){
//         log("successfully opened camera");
//         if (device.platform === "iOS"){
//             // unhide IAB
//             iab.show();
//         }
//     }, function(cameraError){
//         error("error opening camera: "+cameraError);
//         if (device.platform === "iOS"){
//             iab.show();
//         }
//     });
//     if (device.platform === "iOS"){
//         // hide IAB so camera window is in view
//         setTimeout(iab.hide, animationDelay);
//     }
// }


function getIabOpts(){
    var iabOpts;
    if (device.platform === "iOS") {
        iabOpts = 'location=no,toolbar=yes';
        targetWebview = $('input[name=webview]:checked').val();
        if(targetWebview === "wkwebview"){
            iabOpts += ',usewkwebview=yes';
        }

    } else {
        iabOpts = 'location=yes';
    }
    return iabOpts;
}

function onDeviceReady(){
    console.log("deviceready");
    CAMERA = navigator.camera;
    console.log(CAMERA);
    
    osVersion = parseFloat(device.version);

    if( device.platform === "iOS" ) {
        if(window.webkit && window.webkit.messageHandlers ) {
            webView = "WKWebView" ;
        }else{
            webView = "UIWebView" ;
        }
    }else{
        if(navigator.userAgent.toLowerCase().indexOf('crosswalk') > -1) {
            webView = "Crosswalk" ;
        } else {
            webView = "System" ;
        }
    }
    
    $('#platform').html(device.platform + " " + device.version);
    $('#webview').html(webView);
    $('body').addClass(device.platform.toLowerCase());
}

$(document).on('deviceready', onDeviceReady);
